package com.example.hastasm
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Switch
import android.widget.TextView
import com.google.firebase.database.FirebaseDatabase
import org.json.JSONObject
import org.w3c.dom.Text
import java.lang.Exception
import java.net.URL

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        listenSwitches()
        weatherTask().execute()
        supportActionBar!!.hide()
    }

    inner class weatherTask() : AsyncTask<String, Void, String> () {
    override fun onPreExecute() {
        super.onPreExecute()
        //Do all pre loading here.
    }        override fun doInBackground(vararg params: String?): String? {
    var response:String?
    try {
        //response = URL("https://api.darksky.net/forecast/575b00066400fd6a81a81dab62f32a3f/37.8267,-122.4233").readText(Charsets.UTF_8)
        response = URL("http://192.168.0.102:9999/device/d1/getweather").readText(Charsets.UTF_8)
    } catch (e: Exception) {
        Log.e("Error in response: ", e.toString())
        response = null
    }
    return response
}        override fun onPostExecute(result: String?) {
    super.onPostExecute(result)
    try {
        /**
         * {
        "current": {
        "temperature": {
        "time_updated": "1574917211219",
        "unit": "Celcius",
        "value": "25.60"
        },
        "humidity": {
        "time_updated": "1574917212670",
        "unit": "percent",
        "value": "85.40"
        }
        },
        "_id": "5dde8472f04b9c609d921e44",
        "deviceId": "d1",
        "__v": 0
        }
         */
        //TODO: Remove trailing zeroes after the .(dot)
        val jsonObj = JSONObject(result)
        val currently = jsonObj.getJSONObject("current")
        Log.i("Currently: ", currently.toString())
        val temperature = currently.getJSONObject("temperature")
        val temperatureValue = temperature.getString("value")
        val temperatureUnit = temperature.getString("unit")
        val humidity = currently.getJSONObject("humidity")
        val humidityValue = humidity.getString("value")
        val humidityUnit = humidity.getString("unit")
        Log.i("Temperature: ", temperatureValue.toString() + " *"+temperatureUnit);
        Log.i("Humidity: ", humidityValue.toString() + " *"+humidityUnit)
        findViewById<TextView>(R.id.temperatureReportText).text = (temperatureValue + " " + temperatureUnit)
        findViewById<TextView>(R.id.humidityReportText).text = (humidityValue + " " + humidityUnit)
    } catch (e: Exception) {
        Log.e("Error in loading temp: ", e.toString())
        findViewById<TextView>(R.id.temperatureReportText).text = "Error in loading..."
    }
}
}    private fun updateFirebaseAppliancesDb(appliance:String, value:Int){
    val database = FirebaseDatabase.getInstance()
    val myRef = database.getReference(appliance)
    myRef.setValue(value)
}    private fun listenSwitches(){
    val lightSwitch = findViewById<Switch>(R.id.lightToggleSwitch)
    val fanSwitch = findViewById<Switch>(R.id.fanToggleSwitch)
    lightSwitch.setOnCheckedChangeListener { _, isChecked ->
        if(isChecked)
            updateFirebaseAppliancesDb("light",1)
        else
            updateFirebaseAppliancesDb("light", 0)
    }
    fanSwitch.setOnCheckedChangeListener { _, isChecked ->
        if(isChecked)
            updateFirebaseAppliancesDb("fan",1)
        else
            updateFirebaseAppliancesDb("fan", 0)
    }
}

    public fun refreshData(v: View){
        weatherTask().execute()
    }
}